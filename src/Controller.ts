import { LocalServer } from './Server/localserver';
import * as fs from 'fs';
import { CustomeInjector } from './Injector/custom-injector';
import { Page, ClickOptions, Browser,WaitForOptions, ElementHandle } from 'puppeteer-core';
import fetch from 'node-fetch';
import { LCPData, CLSData, FIDData, LCP, FID, CLS,TestElement, WaitOptions, Emulation, Selector,NavigationWaitOptions , LcpAPI} from './types';

import Reporter from './Report/Reporter';
import { Scenario } from './Report/Scenario';

export class Controller {
  protected localServerURL: string = '';
  protected browser: Browser | undefined ;
  protected cPage: Page | undefined;
  protected mode: string= '';
  protected port: number=0;
  protected createReport:boolean = true;
  protected previousAction : string;
  protected currentScenario : Scenario | undefined;
  constructor(){
    this.previousAction = "UNDEFINED"
  }

  init(options: { port?: number; mode?: string; reportTitle?: string, createReport?: boolean }) {
    if(options.port){
      LocalServer.startServer(options.port);
      this.localServerURL = 'http://localhost:' + (options.port || 3000).toString();
      this.port=options.port
    }

    this.mode = options.mode || 'Full';
    if(options.createReport)
      this.createReport = options.createReport
  }

  async setBrowser(browser: Browser, mode?: string) {
    const currentMode = mode ? mode : this.mode;
    this.browser = browser;
    this.cPage= (await this.browser.pages())[0];
    const preloadFile = fs.readFileSync('./node_modules/web-vitals/dist/web-vitals.umd.js', 'utf8');
    const jsToInject =
      preloadFile +
      '\n' +
      CustomeInjector.getCssSelectorJS(currentMode) +
      '\n' +
      CustomeInjector.getSendtoAnalyticsJS(this.port.toString(), currentMode);
    await this.cPage?.evaluateOnNewDocument(jsToInject);
  }

  getPage() {
    return this.cPage
  }

  async setNewPage(mode?: string) {
    const currentMode = mode ? mode : this.mode;
    const context = await this.browser?.createIncognitoBrowserContext();
    this.cPage= await context?.newPage();
    const preloadFile = fs.readFileSync('./node_modules/web-vitals/dist/web-vitals.umd.js', 'utf8');
    const jsToInject =
      preloadFile +
      '\n' +
      CustomeInjector.getCssSelectorJS(currentMode) +
      '\n' +
      CustomeInjector.getSendtoAnalyticsJS(this.port.toString(), currentMode);
    await this.cPage?.evaluateOnNewDocument(jsToInject);
  }

  setCurrentScenario(name: string) {
    this.currentScenario = Reporter.setCurrentScenario(name);
  }

  stopServer() {
    LocalServer.stopServer();
  }

  async goToAndMeasure(
    actionName: string,
    url: string,
    options?: NavigationWaitOptions,
    thinkTime?:number
  ) {
    let error = "No Error"
    await this.beforeAction(actionName, { beforeScreenshot: false });

    if (!options?.hasOwnProperty('waitFor')) await this.cPage?.goto(url,options as WaitForOptions);
    else {
      await this.cPage?.goto(url);
      if ((options as WaitOptions) && (options as WaitOptions).waitFor) {
        await new Promise((resolve) => setTimeout(resolve, (options as WaitOptions).waitFor));
      }
      try{
        if((options as Selector).value){
          if((options as Selector).isXPath)
            await this.cPage?.waitForXPath((options as Selector).value,{visible : true})
          else
            await this.cPage?.waitForSelector((options as Selector).value,{visible : true})
        }
      }catch(e){
        error= `Element ${(options as Selector).value} is not found`
      }
    }
    if(thinkTime)
      await new Promise((resolve) => setTimeout(resolve, thinkTime));
    const response = await this.afterAction(actionName, error === 'No Error', {getCLS: true,getLCP:true });

    return {...response,error};
  }

  /**
   *
   * @param url URL
   * @param actionName Action Name that will be displayed in the report
   * @param element CSS selector or XPath selector
   * @param options navigation options or wait in ms before element click
   */
  async goTo_ClickAndMeasure(
    actionName: string,
    url: string,
    element: Selector | ElementHandle,
    options?: {
      goToOptions?: NavigationWaitOptions,
      clickNavigationOptions? : NavigationWaitOptions,
      clickOptions?: ClickOptions
    },
    thinkTime?:number
  ) {
    let error = 'No Error';
    await this.beforeAction(actionName, { beforeScreenshot: true });

    if ((options?.goToOptions as WaitForOptions).waitUntil ||  (options?.goToOptions as WaitForOptions).timeout)
       await this.cPage?.goto(url, options?.goToOptions as WaitForOptions);
    else {
      await this.cPage?.goto(url);
      if ((options?.goToOptions as WaitOptions).waitFor) {
        await new Promise((resolve) => setTimeout(resolve, (options?.goToOptions as WaitOptions).waitFor));
      }
      try{
        if((options?.goToOptions as Selector).value){
          if((options?.goToOptions as Selector).isXPath)
            await this.cPage?.waitForXPath((options?.goToOptions as Selector).value,{visible : true})
          else
            await this.cPage?.waitForSelector((options?.goToOptions as Selector).value,{visible : true})
        }
      }catch(e){
        error= `Element ${(options?.goToOptions as Selector).value} is not found`
      }
    }

    try {
      let elementHandle : ElementHandle | null | undefined;
      if((element as Selector).isXPath)
        elementHandle=await this.cPage?.waitForXPath((element as Selector).value,{visible:true})
      else if((element as Selector).value)
        elementHandle = await this.cPage?.waitForSelector((element as Selector).value,{visible:true});
      else
        elementHandle = element as ElementHandle
      if (options?.clickNavigationOptions as WaitForOptions) 
        await Promise.all([elementHandle?.click(options?.clickOptions),this.cPage?.waitForNavigation(options?.clickNavigationOptions as WaitForOptions)])
      else
        await elementHandle?.click(options?.clickOptions)
      if ((options?.clickNavigationOptions as WaitOptions) && (options?.clickNavigationOptions as WaitOptions).waitFor) {
        await new Promise((resolve) => setTimeout(resolve, (options?.clickNavigationOptions as WaitOptions).waitFor));
      }
      try{
        if(options?.clickNavigationOptions && (options?.clickNavigationOptions as Selector).value){
          if((options?.clickNavigationOptions as Selector).isXPath)
            await this.cPage?.waitForXPath((options?.clickNavigationOptions as Selector).value,{visible : true})
          else
            await this.cPage?.waitForSelector((options?.clickNavigationOptions as Selector).value,{visible : true})
        }
      }catch(e){
        error= `Element ${(options?.clickNavigationOptions as Selector).value} is not found`
      }
      
    } catch (err) {
        error = 'element with ' + (element as Selector)?.value + ' is not clicked';
        return { error };
    }

    if(thinkTime)
      await new Promise((resolve) => setTimeout(resolve, thinkTime));
    const response = await this.afterAction(actionName, error === 'No Error', {
      getCLS: true,
      getFID: true,
      getLCP: true,
    });

    return {...response,error};
  }

  async goTo_ClickAndMeasureFID(
    actionName: string,
    url: string,
    elementLocation: { x: number; y: number },
    clickAfter: 'domcontentloaded' | 'load' | 'dominteractive'  | 'first-contentful-paint' | 'largest-contentful-paint',
    emulation?: Emulation,
  ) {
    const currentPage = this.cPage
    let error = 'No Error'
    await this.setNewPage('fidOnly');
    await this.setEmulation(emulation);
    await this.beforeAction(actionName, { beforeScreenshot: false, mode: 'fidOnly' });

    switch (clickAfter) {
      case 'domcontentloaded':
        this.cPage?.on('domcontentloaded', async () => {
          await Promise.all([this.cPage?.mouse.click(elementLocation.x, elementLocation.y),this.cPage?.waitForNavigation({waitUntil:'networkidle2'})]);
        });
        await this.cPage?.goto(url, { waitUntil: 'networkidle2' });
        break;
      case 'load':
        this.cPage?.on('load', async () => {
          await Promise.all([this.cPage?.mouse.click(elementLocation.x, elementLocation.y),this.cPage?.waitForNavigation({waitUntil:'networkidle2'})]);
        });
        await this.cPage?.goto(url, { waitUntil: 'networkidle2' });
        break;
      default:
        try {
          const body = { wsEndPoint: this.browser?.wsEndpoint() };
          const response = await fetch(this.localServerURL + '/setwsEndPoint', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(body),
          });
          if (response.status !== 200) throw Error(' Response status is not 200');
        } catch (err) {
           error = 'Could not set wsEndPoint that is required for click before onload event';
        }

        
        await this.cPage?.evaluateOnNewDocument(
          clickAfter === 'dominteractive' ?
            CustomeInjector.getclickEventDOMInteractive_JS(this.port.toString(),elementLocation)
          : clickAfter === 'largest-contentful-paint' ? CustomeInjector.getclickEventPostLCP_JS(this.port.toString(), elementLocation) :
            CustomeInjector.getclickEventPostPaintAPI_JS(this.port.toString(), elementLocation,clickAfter)
        );

        await this.cPage?.goto(url,{waitUntil:'networkidle2'});
    }

    const result = await this.afterAction(actionName, error === 'No Error', {
      getFID: true,
      getNavigation: true,
    });

    await this.cPage?.close();
    this.cPage= currentPage;

    return {...result,error};
  }

  async clickAndMeasure(
    actionName: string,
    element: Selector | ElementHandle,
    options?:{
      clickOptions?: ClickOptions,
      navigationOptions?:NavigationWaitOptions
    },
    thinkTime?:number
  ) {
    let error = 'No Error';

    await this.beforeAction(actionName, { beforeScreenshot: true });

    try {
      let elementHandle;
      if((element as Selector).isXPath)
         elementHandle=await this.cPage?.waitForXPath((element as Selector).value,{visible:true})
      else if((element as Selector).value)
        elementHandle = await this.cPage?.waitForSelector((element as Selector).value,{visible:true});
      else
        elementHandle =  element as ElementHandle
      if (options?.navigationOptions && ((options?.navigationOptions as WaitForOptions).waitUntil ||  (options?.navigationOptions as WaitForOptions).timeout))
        await Promise.all([elementHandle?.click(options?.clickOptions),this.cPage?.waitForNavigation(options?.navigationOptions as WaitForOptions)])
      else
        await elementHandle?.click(options?.clickOptions)
      if ((options?.navigationOptions as WaitOptions).waitFor) {
        await new Promise((resolve) => setTimeout(resolve, (options?.navigationOptions as WaitOptions).waitFor));
      }
      try{
        if(options?.navigationOptions && (options?.navigationOptions as Selector).value){
          if((options?.navigationOptions as Selector).isXPath)
            await this.cPage?.waitForXPath((options?.navigationOptions as Selector).value,{visible : true})
          else
            await this.cPage?.waitForSelector((options?.navigationOptions as Selector).value,{visible : true})
        }
      }catch(e){
        error= `After Action Navigation : Element ${(options?.navigationOptions as Selector).value} is not found`
      }
      
    } catch (err) {
      if(!error.startsWith('After Action Navigation'))
        error = `element with ${(element as Selector)?.value} is not clicked`;
    }

    if(thinkTime)
      await new Promise((resolve) => setTimeout(resolve, thinkTime));
    const response = await this.afterAction(actionName, error === 'No Error', { getCLS: true, getFID: true, getLCP: true});

    return { ...response, error };
  }

  async selectAndMeasure(
    actionName: string,
    element: Selector | ElementHandle,
    selection : string,
    navigationOptions?: NavigationWaitOptions,
    thinkTime? : number
  ) {
    let error = 'No Error';

    await this.beforeAction(actionName, { beforeScreenshot: true });

    try {
      let elementHandle;
      if((element as Selector).isXPath)
        elementHandle=await this.cPage?.waitForXPath((element as Selector).value,{visible:true})
      else if((element as Selector).value)
        elementHandle = await this.cPage?.waitForSelector((element as Selector).value,{visible:true});
      else
        elementHandle =  element as ElementHandle
      await elementHandle?.click();
      if ((navigationOptions as WaitForOptions).timeout ||  (navigationOptions as WaitForOptions).waitUntil)
        await Promise.all([elementHandle?.select(selection),this.cPage?.waitForNavigation(navigationOptions as WaitForOptions)]);
        else
      await elementHandle?.select(selection)
      if ((navigationOptions as WaitOptions).waitFor) {
        await new Promise((resolve) => setTimeout(resolve, (navigationOptions as WaitOptions).waitFor));
      }
      try{
        if((navigationOptions as Selector).value){
          if((navigationOptions as Selector).isXPath)
            await this.cPage?.waitForXPath((navigationOptions as Selector).value,{visible : true})
          else
            await this.cPage?.waitForSelector((navigationOptions as Selector).value,{visible : true})
        }
      }catch(e){
        error= `Element ${(navigationOptions as Selector).value} is not found`
      }
    } catch (err) {
      error = ' option ' + selection+ ' is not selected for dropdown ' + (element as Selector)?.value;
      return { error };
    }

    if(thinkTime)
      await new Promise((resolve) => setTimeout(resolve, thinkTime));
    const response = await this.afterAction(actionName, error === 'No Error', { getCLS: true, getFID: true, getLCP: true});

    return { ...response, error };
  }

  async typeAndMeasure(
    actionName: string,
    element: Selector | ElementHandle,
    input : {text:string, pressEnter:boolean},
    navigationOptions?: NavigationWaitOptions,
    thinkTime?:number
  ) {
    let error = 'No Error';

    await this.beforeAction(actionName, { beforeScreenshot: true });

    try {
      let elementHandle;
      if((element as Selector).isXPath)
        elementHandle=await this.cPage?.waitForXPath((element as Selector).value,{visible:true})
      else if((element as Selector).value)
        elementHandle = await this.cPage?.waitForSelector((element as Selector).value,{visible:true});
      else
        elementHandle =  element as ElementHandle
      await elementHandle?.click();
      if ((navigationOptions as WaitForOptions).timeout ||  (navigationOptions as WaitForOptions).waitUntil){
        await elementHandle?.type(input.text,{delay:200})
        if(input.pressEnter)
          await Promise.all([elementHandle?.press('Enter'),this.cPage?.waitForNavigation(navigationOptions as WaitForOptions)]);
        else
          await this.cPage?.waitForNavigation(navigationOptions as WaitForOptions);
      }else{
        await elementHandle?.type(input.text,{delay:100})
        if(input.pressEnter)
          await elementHandle?.press('Enter')
      }
      if ((navigationOptions as WaitOptions).waitFor) {
        await new Promise((resolve) => setTimeout(resolve, (navigationOptions as WaitOptions).waitFor));
      }
      try{
        if((navigationOptions as Selector).value){
          if((navigationOptions as Selector).isXPath)
            await this.cPage?.waitForXPath((navigationOptions as Selector).value,{visible : true})
          else
            await this.cPage?.waitForSelector((navigationOptions as Selector).value,{visible : true})
        }
      }catch(e){
        error= `After Action Navigation : Element ${(navigationOptions as Selector).value} is not found`
      }
    } catch (err) {
      if(!error.startsWith('After Action Navigation'))
        error = `selector ${(element as Selector)?.value} is not found `
    }

    if(thinkTime)
      await new Promise((resolve) => setTimeout(resolve, thinkTime));
    const response = await this.afterAction(actionName, error === 'No Error', { getCLS: true, getFID: true, getLCP: true});

    return { ...response, error };
  }

  async scrollAndMeasure(actionName: string, option: { pixels? : {down: number; right: number }, elementInToView? : Selector | ElementHandle},thinkTime?:number) {
    let error = 'No Error'
    await this.beforeAction(actionName, { beforeScreenshot: false });
    let elementHandle;
    try{
      if(option.elementInToView){
        if ((option.elementInToView as Selector).isXPath) 
          elementHandle = this.cPage? (await this.cPage?.$x((option.elementInToView as Selector).value))[0] : undefined;
        else if((option.elementInToView as Selector).value)
          elementHandle = await this.cPage?.$((option.elementInToView as Selector).value); 
        else
          elementHandle = option.elementInToView as ElementHandle
      }
    let verticalCounter = 0
    
    for (let i = 0; i < 20; i++) {
      let modifier = 1
      if(option.pixels){
        if(verticalCounter>=Math.abs(option.pixels.down))
          break;
        modifier = option.pixels.down > 0 ? 1 : -1
      }else if(elementHandle) {
        if(await elementHandle.isIntersectingViewport()){
          break;
        }
      }
      await this.cPage?.evaluate((isNegative : number) => {
        window.scrollBy(0 , 200*isNegative);
      },modifier);
      verticalCounter+=200
      await new Promise((resolve) => setTimeout(resolve, 100));
    }

    let horizontalCounter = 0
    for (let i = 0; i < 20; i++) {
      let modifier = 1
      if(option.pixels){
        if(horizontalCounter>=Math.abs(option.pixels.right))
          break;
        modifier = option.pixels.right > 0 ? 1 : -1
      }else if(elementHandle) {
        if(await elementHandle.isIntersectingViewport()){
          break;
        }
       
      }
      await this.cPage?.evaluate((isNegative:number) => {
        window.scrollBy(200*isNegative, 0);
      },modifier);
      horizontalCounter+=200
      await new Promise((resolve) => setTimeout(resolve, 100));
    }
  }catch(e){
    error = e.message
  }

    if(thinkTime)
      await new Promise((resolve) => setTimeout(resolve, thinkTime));

    const response = await this.afterAction(actionName, error === 'No Error', { getCLS: true});

    return {...response,error};
  }

  async hoverAndMeasure(actionName: string, element: Selector | ElementHandle,thinkTime?:number) {
    let error = 'No Error'
    await this.beforeAction(actionName, { beforeScreenshot: false });
    try{
      let elementHandle;
      if((element as Selector).isXPath)
        elementHandle=await this.cPage?.waitForXPath((element as Selector).value,{visible:true})
      else if((element as Selector).value)
        elementHandle = await this.cPage?.waitForSelector((element as Selector).value,{visible:true});
      else
        elementHandle =  element as ElementHandle
      
      await elementHandle?.hover()
    }catch(e){
      error = e.message
    }

    if(thinkTime)
      await new Promise((resolve) => setTimeout(resolve, thinkTime));

    const response = await this.afterAction(actionName, error === 'No Error', { getCLS: true});

    return {...response,error};
  }

  async getCLS(actionName?: string) {
   
    let clsDataArray: CLSData[];
    const returnData: CLS[] = [];
    let localStorageData 
    for(let i=0;i<2;i++){
      localStorageData = await this.cPage?.evaluate(() => {
       return localStorage.getItem("CLS");
      });
    if(!localStorageData || localStorageData === undefined)
      await new Promise((resolve) => setTimeout(resolve, 100));
    else{
      await this.cPage?.evaluate(() => {
        localStorage.CLS=[];
      });
      break;
    }
  }
    if(!localStorageData || localStorageData === undefined)
      return undefined
    clsDataArray = JSON.parse(localStorageData)

    for(const clsData of clsDataArray){

    const allElements: TestElement[] = [];
 
      const entries = clsData?.entries?.[0].sources || [];
      for (const entry of entries) {
        allElements.push(entry);
      }
    
    const clsElements = allElements.reduce((accumulator: TestElement[], currentValue) => {
      if (!accumulator.find((obj) => obj.cssSelector === currentValue.cssSelector)) {
        accumulator.push(currentValue);
      }
      return accumulator;
    }, []);

    for (const newElement of clsElements) {
      try {
        const sources = newElement;
        if (sources.cssSelector) {
          const newElementHandle = await this.cPage?.waitForSelector(sources.cssSelector, { timeout: 100 });
          if (newElementHandle) {
            const newLocation = await newElementHandle.boundingBox();
            if (newLocation) {
              newElement.newLocation = newLocation;
            }
          }
        }
      } catch (err) {
        continue;
      }
    }

    const cls: CLS = { URL: clsData?.URL,actionName:clsData?.actionName,value: clsData?.value || 0, elements: clsElements };
    returnData.push(cls)
  }
    return returnData;
  }

  async getFID(actionName?: string) {
    let fidEntry: FIDData
    let localStorageData 
    for(let i=0;i<2;i++){
      localStorageData = await this.cPage?.evaluate(() => {
        return localStorage.getItem("FID");
      });
      if(!localStorageData || localStorageData === undefined)
        await new Promise((resolve) => setTimeout(resolve, 100));
      else{
        await this.cPage?.evaluate(() => {
          localStorage.FID=[];
        });
        break;
      }
    }
    if(!localStorageData || localStorageData === undefined)
        return undefined
    fidEntry = JSON.parse(localStorageData)[0]

    if (fidEntry) return { URL:fidEntry.URL,actionName: fidEntry.actionName,value: fidEntry.value, startTime: fidEntry.entries?.[0].startTime };
      return undefined;
  }

  async getLCP(actionName?: string) {
    let lcpPOEntry : LCPData;
    let localStorageData;
    let localStoragePOData;
    const lcpReturn : LCP[] = [];
    for(let i=0;i<2;i++){
      localStorageData = await this.cPage?.evaluate(() => {
      return localStorage.getItem("LCP");
       });
      if(!localStorageData || localStorageData === undefined)
       await new Promise((resolve) => setTimeout(resolve, 100));
     else{
      await this.cPage?.evaluate(() => {
        localStorage.setItem("LCP",'');
      });
       break;
     }
    }
    for(let i=0;i<2;i++){
      localStoragePOData = await this.cPage?.evaluate(() => {
      return localStorage.getItem("LCP-PO");
       });
      if(!localStoragePOData || localStoragePOData === undefined)
       await new Promise((resolve) => setTimeout(resolve, 100));
     else{
      await this.cPage?.evaluate(() => {
        localStorage["LCP-PO"]=[];
      });
       break;
     }
    }
    if((!localStorageData || localStorageData === undefined)&&(!localStoragePOData || localStoragePOData === undefined))
      return undefined

    if(localStorageData !== undefined  && localStorageData !=null && localStorageData){
      const lcpEntries:LcpAPI[] = JSON.parse(localStorageData)

      for(const lcpEvent of lcpEntries){
        const lcpEntry = lcpEvent.entries?.pop();
        const lcpElement : TestElement={
          cssSelector: lcpEntry?.cssSelector,
          location: lcpEntry?.location
        };
      
        const lcp: LCP = {
          URL:lcpEvent.URL,
          elementURL : lcpEntry?.url,
          actionName : lcpEvent.actionName,
          value: lcpEvent.value,
          element: lcpElement,
          engine:'web-vitals',
          loadTime:lcpEntry?.loadTime,
          renderTime:lcpEntry?.renderTime
        };

        lcpReturn.push(lcp);
      }
    }

    if(localStoragePOData !== undefined  && localStoragePOData !=null && localStoragePOData){
      lcpPOEntry = JSON.parse(localStoragePOData)
      const lcpElement : TestElement={
        cssSelector: lcpPOEntry.cssSelector,
        location: lcpPOEntry.location
      };
    
      const lcp: LCP = {
        URL:lcpPOEntry.URL,
        elementURL : lcpPOEntry.url,
        actionName : lcpPOEntry.actionName,
        value: lcpPOEntry.value,
        element: lcpElement,
        engine: 'PO',
        loadTime: lcpPOEntry.loadTime,
        renderTime: lcpPOEntry.renderTime
      };

      lcpReturn.push(lcp);
      }
 
    return lcpReturn;
  }

  async getPerformanceTiming() {
    const performanceTiming = JSON.parse(
      this.cPage? await this.cPage.evaluate(() => JSON.stringify(performance.getEntriesByType('navigation'))) : ''
    );
    return {
      requestStart: performanceTiming?.[0].requestStart,
      responseStart: performanceTiming?.[0].responseStart,
      responseEnd: performanceTiming?.[0].responseEnd,
      domInteractive: performanceTiming?.[0].domInteractive,
      domContentLoadedEventStart: performanceTiming?.[0].domContentLoadedEventStart,
      domContentLoadedEventEnd: performanceTiming?.[0].domContentLoadedEventEnd,
      domComplete: performanceTiming?.[0].domComplete,
      loadEventStart: performanceTiming?.[0].loadEventStart,
      loadEventEnd: performanceTiming?.[0].loadEventEnd,
    };
  }

  async beforeAction(actionName: string, options: { beforeScreenshot: boolean; mode?: string }) {
    try{
    await this.cPage?.evaluate((webVitalsActionName : string,previousActionName : string) => {
     localStorage["WebVitalsTest-ActionName"] = webVitalsActionName
     localStorage["WebVitalsTest-PreviousAction"] = previousActionName
     },actionName,this.previousAction);
    }catch(e){
      await this.cPage?.evaluateOnNewDocument((webVitalsActionName : string,previousActionName : string) => {
        if(location.hostname.length>0){
          if(localStorage.getItem("WebVitalsTest-ActionName")===null){
            localStorage["WebVitalsTest-ActionName"] = webVitalsActionName
            localStorage["WebVitalsTest-PreviousAction"] = previousActionName
          }
        }
        },actionName,this.previousAction);
    }
  }

  async afterAction(
    actionName: string,
    status: boolean,
    options?: { getCLS?: boolean; getLCP?: boolean; getFID?: boolean; getNavigation?: boolean; mode?: string },
  ) {
    const appliedMode = options?.mode ? options.mode : this.mode;
    let navigationTiming;
    if (appliedMode === 'full') {
      navigationTiming = await this.getPerformanceTiming();
    }

    const clsData: CLS[] | undefined = options?.getCLS ? await this.getCLS() : undefined;
    const lcpData: LCP[] | undefined = options?.getLCP ? await this.getLCP() : undefined;
    const fidData: FID | undefined = options?.getFID ? await this.getFID() : undefined;

    if(this.createReport){
      const currentURL = this.cPage?.url();
      if(this.currentScenario === undefined) this.setCurrentScenario("Test 1")
      this.currentScenario?.addEntry({
            actionName,
            status,
            url : currentURL,
            CLS: clsData,
            FID: fidData,
            LCP: lcpData,
            navigationTimings: navigationTiming,
      });
    }

    this.previousAction = actionName;
    return { action : actionName , status, CLS: clsData, FID: fidData, LCP: lcpData, navigation: navigationTiming };
  }

  async setEmulation(emulation?: Emulation) {
    if (emulation) {
      if (emulation.userAgent && emulation.viewport)
        this.cPage?.emulate({
          userAgent: emulation.userAgent,
          viewport: emulation.viewport,
        });

      if (emulation.network || emulation.cpuThrottle) {
        const client = await this.cPage?.target().createCDPSession();
        if (emulation.network){
          await client?.send('Network.enable');
          await client?.send('Network.emulateNetworkConditions', emulation.network);
        } 
        if (emulation.cpuThrottle) await client?.send('Emulation.setCPUThrottlingRate', emulation.cpuThrottle);
      }
    }
  }

  async getResults(scenarioName?: string){
    return Reporter.getResults(scenarioName)
  }

  async getBrowser(){
    return this.browser
  }
}
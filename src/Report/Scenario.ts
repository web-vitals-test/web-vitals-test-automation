import { Aggregation, ReportEntry, ReportLine, Experience } from './../types';
import { URLReport } from "./URLReport"


export class Scenario {
  private name: string;
  private URLList: URLReport[] = [];
  private tags: string[];
  constructor(name: string, index: number) {
    this.name = name;
    this.tags = [];
  }

  setURLList(urlList: URLReport[]){
    this.URLList = urlList
  }

  getName() {
    return this.name;
  }

  addTags(tags: string | string[]){
    if(Array.isArray(tags)){
      this.tags = tags.slice();
    }else{
      this.tags.push(tags)
    }
  }

  getTags(){
    return this.tags
  }

  addEntry(reportEntry: ReportEntry) {
    let lastURLReport = this.URLList?.length > 0 ? this.URLList[this.URLList?.length-1] : undefined
    let secondLastURLReport = this.URLList?.length > 1 ?  this.URLList[this.URLList?.length-2] : undefined

    const clsEntryForAction = reportEntry.CLS?.filter(clsEntry => clsEntry.actionName === reportEntry.actionName)
    if(!(clsEntryForAction&&clsEntryForAction.length>0)){
      if(reportEntry.CLS)
        reportEntry.CLS.push({actionName:reportEntry.actionName,value:0,URL:reportEntry.url})
      else
        reportEntry.CLS=[{actionName:reportEntry.actionName,value:0,URL:reportEntry.url}]
      }
    if (reportEntry.CLS) {
      for(const clsEntry of reportEntry.CLS){
        if(clsEntry.actionName){
          let reportLineToUpdate : URLReport | undefined
          if(clsEntry.URL === secondLastURLReport?.getURL()){
            reportLineToUpdate = secondLastURLReport
          }else if(clsEntry.URL === lastURLReport?.getURL()){
            reportLineToUpdate = lastURLReport
          }else{
            reportLineToUpdate = new URLReport(clsEntry.URL || "NO URL")
            this.URLList.push(reportLineToUpdate)
            secondLastURLReport = lastURLReport
            lastURLReport = reportLineToUpdate
          }
          if(reportLineToUpdate){
            const clsToUpdate = {
                value: clsEntry.value,
                experience:
                clsEntry.value <= 0.1
                    ? Experience.fast
                    : clsEntry.value <= 0.25
                    ? Experience.average
                    : Experience.poor,
                elements: clsEntry.elements,
                screenshot : clsEntry.screenshot
            }; 

            reportLineToUpdate.addEntry({actionName: clsEntry.actionName,CLS:clsToUpdate,status:reportEntry.status})
          }
        }
      }
    }

    if (reportEntry.LCP) {
      for(const lcpEntry of reportEntry.LCP){
        if(lcpEntry.actionName){
          let reportLineToUpdate : URLReport | undefined
          if(lcpEntry.URL === secondLastURLReport?.getURL()){
            reportLineToUpdate = secondLastURLReport
          }else if(lcpEntry.URL === lastURLReport?.getURL()){
            reportLineToUpdate = lastURLReport
          }else{
              reportLineToUpdate = new URLReport(lcpEntry.URL || "NO URL")
              this.URLList.push(reportLineToUpdate)
              secondLastURLReport = lastURLReport
              lastURLReport = reportLineToUpdate
          }
          if(reportLineToUpdate){
            const lcpToUpdate = {
              value: lcpEntry.value,
              experience:
              lcpEntry.value <= 2500
                  ? Experience.fast
                  : lcpEntry.value <= 4000
                  ? Experience.average
                  : Experience.poor,
              element: lcpEntry.element,
              screenshot : lcpEntry.screenshot
            };
            reportLineToUpdate.addEntry({actionName:lcpEntry.actionName, LCP :lcpToUpdate,status:reportEntry.status})
          }
        }
      }
    }

    if (reportEntry.FID) {
      const fidEntry = reportEntry.FID
      if(fidEntry.actionName){
        let reportLineToUpdate : URLReport | undefined
        if(fidEntry.URL === secondLastURLReport?.getURL()){
          reportLineToUpdate = secondLastURLReport
        }else if(fidEntry.URL === lastURLReport?.getURL()){
          reportLineToUpdate = lastURLReport
        }else{
          reportLineToUpdate = new URLReport(fidEntry.URL || "NO URL")
          this.URLList.push(reportLineToUpdate)
          secondLastURLReport = lastURLReport
          lastURLReport = reportLineToUpdate
        }
        if(reportLineToUpdate){
          const fidToUpdate= {
            value: Number.parseFloat(reportEntry.FID.value.toFixed(2)),
            startTime: reportEntry.FID.startTime ? Number.parseInt(reportEntry.FID.startTime.toFixed(0),10) : undefined,
            experience:
              reportEntry.FID.value <= 100
                ? Experience.fast
                : reportEntry.FID.value <= 300
                ? Experience.average
                : Experience.poor,
          };
          reportLineToUpdate.addEntry({actionName:fidEntry.actionName,FID:fidToUpdate,status:reportEntry.status})
        }
      }
    }

    if (reportEntry.navigationTimings) lastURLReport?.setNavigationTimings(reportEntry.navigationTimings);
  }

  getEntryList() {
    return this.URLList;
  }

  getAggregatedData() {
    const overAll: Aggregation = { fast: 0, average: 0, poor: 0 };
    const clsAggregation: Aggregation = { fast: 0, average: 0, poor: 0 };
    const lcpAggregation: Aggregation = { fast: 0, average: 0, poor: 0 };
    const fidAggregation: Aggregation = { fast: 0, average: 0, poor: 0 };

    for (const entryItem of this.URLList) {
      const aggregatedData = entryItem.getAggregatedData();

      clsAggregation[Experience.fast] += aggregatedData.CLS[Experience.fast]
      clsAggregation[Experience.average] += aggregatedData.CLS[Experience.average]
      clsAggregation[Experience.poor] += aggregatedData.CLS[Experience.poor]

      lcpAggregation[Experience.fast] += aggregatedData.LCP[Experience.fast]
      lcpAggregation[Experience.average] += aggregatedData.LCP[Experience.average]
      lcpAggregation[Experience.poor] += aggregatedData.LCP[Experience.poor]

      fidAggregation[Experience.fast] += aggregatedData.FID[Experience.fast]
      fidAggregation[Experience.average] += aggregatedData.FID[Experience.average]
      fidAggregation[Experience.poor] += aggregatedData.FID[Experience.poor]
    }

    overAll[Experience.fast] = clsAggregation[Experience.fast] + lcpAggregation[Experience.fast] + fidAggregation[Experience.fast]
    overAll[Experience.average] = clsAggregation[Experience.average] + lcpAggregation[Experience.average] + fidAggregation[Experience.average]
    overAll[Experience.poor] = clsAggregation[Experience.poor] + lcpAggregation[Experience.poor] + fidAggregation[Experience.poor]

    return { overall: overAll, CLS: clsAggregation, LCP: lcpAggregation, FID: fidAggregation};
  }

}

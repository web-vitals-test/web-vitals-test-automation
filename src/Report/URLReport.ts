import { Aggregation, URLReportLine, ReportLine, Experience, Timings, CLS, FID, LCP } from './../types';
import { ActionItem } from './ActionItem';

export class URLReport implements URLReportLine{

    URL: string;
    CLS?: CLS | undefined;
    LCP?: LCP | undefined;
    FID?: FID | undefined;
    actionList : ActionItem[];
    navigationTimings?: Timings | undefined;
    screenshot?: string | undefined;

    constructor(url:string){
        this.URL = url;
        this.actionList=[];
    }

    addEntry(reportLine: ActionItem) {
        const existingReportLine = this.actionList?.filter(action => action.actionName === reportLine.actionName)
        let actionLine: ActionItem
        if( existingReportLine&& existingReportLine.length>0)
         actionLine  =existingReportLine[0]
        else{
         actionLine = new ActionItem(reportLine.actionName)
         this.actionList.push(actionLine)
        }
    
        if (reportLine.CLS) {
            const clsEntry = reportLine.CLS
            actionLine.CLS = {
                value: clsEntry.value,
                experience:
                clsEntry.value <= 0.1
                    ? Experience.fast
                    : clsEntry.value <= 0.25
                    ? Experience.average
                    : Experience.poor,
                elements: clsEntry.elements,
                screenshot : clsEntry.screenshot
            };

            if(this.CLS){
                if(this.CLS.value < reportLine.CLS.value)
                    this.CLS = reportLine.CLS
            }else{
                this.CLS = reportLine.CLS
            }
        }
    
        if (reportLine.LCP) {
            if(actionLine.LCP?.engine !== 'web-vitals'){
                actionLine.LCP = {
                    value: reportLine.LCP.value,
                    experience:
                        reportLine.LCP.value <= 2500
                        ? Experience.fast
                        : reportLine.LCP.value <= 4000
                        ? Experience.average
                        : Experience.poor,
                    element: reportLine.LCP.element,
                    screenshot : reportLine.LCP.screenshot
                };
                this.LCP = this.LCP? this.LCP : reportLine.LCP
            }
        }
    
        if (reportLine.FID) {
          actionLine.FID = {
            value: Number.parseFloat(reportLine.FID.value.toFixed(2)),
            startTime: reportLine.FID.startTime ? Number.parseInt(reportLine.FID.startTime.toFixed(0),10) : undefined,
            experience:
                reportLine.FID.value <= 100
                ? Experience.fast
                : reportLine.FID.value <= 300
                ? Experience.average
                : Experience.poor,
          };

          this.FID = this.FID? this.FID : reportLine.FID
        }
        actionLine.status=reportLine.status
        if (reportLine.navigationTimings) actionLine.navigationTimings = reportLine.navigationTimings;
    }
    
    getURL(){
        return this.URL;
    }

    getEntryList() {
        return this.actionList;
    }
    
    getAggregatedData() {
        const overAll: Aggregation = { fast: 0, average: 0, poor: 0 };
        const clsAggregation: Aggregation = { fast: 0, average: 0, poor: 0 };
        const lcpAggregation: Aggregation = { fast: 0, average: 0, poor: 0 };
        const fidAggregation: Aggregation = { fast: 0, average: 0, poor: 0 };
    
        if(this.actionList){
            for (const entryItem of this.actionList) {
            if (entryItem.CLS && entryItem.CLS.experience) {
                clsAggregation[entryItem.CLS.experience]++;
            }
            if (entryItem.LCP && entryItem.LCP.experience) {
                lcpAggregation[entryItem.LCP.experience]++;
                overAll[entryItem.LCP.experience]++;
            }
            if (entryItem.FID && entryItem.FID.experience) {
                fidAggregation[entryItem.FID.experience]++;
                overAll[entryItem.FID.experience]++;
            }
            }
        }
    
        return { overall: overAll, CLS: clsAggregation, LCP: lcpAggregation, FID: fidAggregation };
    }

    setNavigationTimings(timings : Timings){
       this.navigationTimings = timings
    }

    getReportLineWith(actionName?:string){
       const reportLines = this.actionList.filter(entry => entry.actionName ===actionName)
       return reportLines.length > 0? reportLines[reportLines.length-1] : undefined
    }

}
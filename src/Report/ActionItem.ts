import { CLS, FID, LCP, ReportLine, Timings } from "../types";

export class ActionItem implements ReportLine{
    CLS?: CLS ;
    status?:boolean;
    LCP?: LCP ;
    FID?: FID ;
    navigationTimings?: Timings ;
    screenshot?: string;
    actionName : string;

    constructor(actionName:string){
        this.actionName = actionName
    }
}
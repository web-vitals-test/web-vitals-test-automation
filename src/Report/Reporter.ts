import * as fs from 'fs';
import { Aggregation, ReportEntry } from './../types';
import { Scenario } from './Scenario';

class Reporter {
 
  private entryList: Scenario[] = [];

  constructor(){
    if (!fs.existsSync('report')) fs.mkdirSync('report');
  }

  getCurrentIndex() {
    const size = this.entryList.length;
    return size;
  }

  setCurrentScenario(name: string) {
    const scenario = new Scenario(name, this.entryList.length);
    this.entryList.push(scenario);
    return scenario;
  }

  getCurrentScenario(){
    if(this.entryList.length > 0)
      return this.entryList[this.entryList.length-1]
    else
      return undefined;
  }

  getEntryList() {
    return this.entryList;
  }

  getAggregatedData() {
    const overAll: Aggregation = { fast: 0, average: 0, poor: 0 };
    const clsAggregation: Aggregation = { fast: 0, average: 0, poor: 0 };
    const lcpAggregation: Aggregation = { fast: 0, average: 0, poor: 0 };
    const fidAggregation: Aggregation = { fast: 0, average: 0, poor: 0 };

    for (const entry of this.entryList) {
      const entryAggregation = entry.getAggregatedData();
      clsAggregation.fast += entryAggregation.CLS.fast;
      clsAggregation.average += entryAggregation.CLS.average;
      clsAggregation.poor += entryAggregation.CLS.poor;
      lcpAggregation.fast += entryAggregation.LCP.fast;
      lcpAggregation.average += entryAggregation.LCP.average;
      lcpAggregation.poor += entryAggregation.LCP.poor;
      fidAggregation.fast += entryAggregation.FID.fast;
      fidAggregation.average += entryAggregation.FID.average;
      fidAggregation.poor += entryAggregation.FID.poor;
      overAll.fast += entryAggregation.overall.fast;
      overAll.average += entryAggregation.overall.average;
      overAll.poor += entryAggregation.overall.poor;
    }

    return { overall: overAll, CLS: clsAggregation, LCP: lcpAggregation, FID: fidAggregation };
  }

  exportJSON() {
    const outputJSON = {aggregation : this.getAggregatedData() , results : this.entryList}
    fs.writeFileSync('report/webvitals.json', JSON.stringify(outputJSON,null,2));
  }

  getResults(scenarioName?:string){
    if(scenarioName){
      const scenarioResults = this.entryList.filter(entry => entry.getName() === scenarioName)
      if(scenarioResults && scenarioResults.length>0)
        return scenarioResults
      else
        return undefined
    }else{
      return this.entryList;
    }   
  }

}

export default new Reporter()

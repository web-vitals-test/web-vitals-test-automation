import { expect } from 'chai';
import { WebVitalsTest } from '../../index';

describe('Click', function () {

  it('ClickAndMeasure', async () => {
    await WebVitalsTest.goToAndMeasure('goto','https://www.bbc.co.uk/', { waitUntil: 'networkidle2' });
    await new Promise((resolve) => setTimeout(resolve, 2000));
    let results = await WebVitalsTest.clickAndMeasure('click',{ value: '//*[text()="Welcome to the BBC"]',isXPath:true },{navigationOptions : {waitFor : 2000}} );
    expect(results).to.have.property('FID').with.property('value').greaterThan(0);
    expect(results.status).true
  });
});

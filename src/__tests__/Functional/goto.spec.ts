import { expect } from 'chai';
import { WebVitalsTest } from '../../index';

describe('goto', function () {
  it('Desktop', async () => {
    let results = await WebVitalsTest.goToAndMeasure('InitialLoad', 'https://www.papergang.co.uk/',{ waitUntil:'networkidle2' });
    expect(results).to.have.property('CLS');
    expect(results).to.have.property('LCP');
    expect(results.status).true
  });
});

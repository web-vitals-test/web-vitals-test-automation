import { expect } from 'chai';
import { WebVitalsTest } from '../../index';

describe('Hover', function () {

  it('HoverAndMeasure', async () => {
    await WebVitalsTest.goToAndMeasure('goto','https://www.next.co.uk/', { waitUntil: 'networkidle2' });
    await new Promise((resolve) => setTimeout(resolve, 2000));
    let results = await WebVitalsTest.hoverAndMeasure('hover',{ value: '[data-department="WOMEN"]' },2000 );
    expect(results.status).true
  });
});

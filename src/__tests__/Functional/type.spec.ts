import { expect } from 'chai';
import { ElementHandle } from 'puppeteer-core';
import { WebVitalsTest } from '../../index';

describe('Type', function () {

  it('TypeAndMeasure by selector', async () => {
    await WebVitalsTest.goToAndMeasure('goto','https://www.next.co.uk/', { waitUntil: 'networkidle2' });
    await new Promise((resolve) => setTimeout(resolve, 2000));
    let results = await WebVitalsTest.typeAndMeasure('type',{ value: '#sli_search_1' },{text:'shirt',pressEnter:true},{waitUntil:'networkidle2'} );
    expect(results).to.have.property('FID').with.property('value').greaterThan(0);
    expect(results.status).true
  });
  it('TypeAndMeasure by elementHandle', async () => {
    await WebVitalsTest.goToAndMeasure('goto','https://www.next.co.uk/', { waitUntil: 'networkidle2' });
    await new Promise((resolve) => setTimeout(resolve, 2000));
    const elementHandle = await WebVitalsTest.getPage()?.$("#sli_search_1")
    let results = await WebVitalsTest.typeAndMeasure('type',elementHandle as ElementHandle,{text:'shirt',pressEnter:true},{waitUntil:'networkidle2'} );
    expect(results).to.have.property('FID').with.property('value').greaterThan(0);
    expect(results.status).true
  });

  it('TypeAndMeasure SPA', async () => {
    let results=await WebVitalsTest.goToAndMeasure('goto','https://www.dunelm.com/product/frea-cylinder-shade-15cm-1000169156', { waitUntil: 'networkidle2' });
    await new Promise((resolve) => setTimeout(resolve, 2000));
    const elementHandle = await WebVitalsTest.getPage()?.$('[data-testid="headerSearchInput-desktop"]')
    results = await WebVitalsTest.typeAndMeasure('type',elementHandle as ElementHandle,{text:'lamp',pressEnter:true},{waitUntil:'networkidle2'} );
    expect(results).to.have.property('FID').with.property('value').greaterThan(0);
    expect(results.status).true
  });
});

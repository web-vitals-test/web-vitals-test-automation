
import { Browser } from 'puppeteer-core';
import { WebVitalsTest } from '../../index';
import * as puppeteer from 'puppeteer';

let browser: unknown;
before(function() {
  WebVitalsTest.init({ port: 3001, mode: process.env.MODE || 'full' });
});

after(function () {
  WebVitalsTest.stopServer();
});

beforeEach(async function () {
  browser = await puppeteer.launch({
    headless: false,
    defaultViewport: { height: 1200, width: 1950 },
    args: ['--start-maximized'],
  });
  await WebVitalsTest.setBrowser(browser as Browser);
});

afterEach(function () {
  (browser as Browser).close();
});

import { expect } from 'chai';
import { WebVitalsTest } from '../../index';

describe('FID', function () {
  it('GoTo_ClickAndMeasureFID domContentLoaded', async () => {
   // To Find Website to test this
  });

  it('GoTo_ClickAndMeasureFID load', async () => {
    // To Find Website to test this
  });

  it('GoTo_ClickAndMeasureFID FCP', async () => {
    // To Find Website to test this
  });
  it('GoTo_ClickAndMeasureFID LCP', async () => {
    await WebVitalsTest.goToAndMeasure('goto','https://www.dunelm.com/product/frea-cylinder-shade-15cm-1000169156', { waitUntil: 'networkidle2' });
    const elementHandle =  await WebVitalsTest.getPage()?.$('[data-testid="mainImage"]')
    const location = await elementHandle?.boundingBox();
    if(location?.x&&location?.y){
      let results = await WebVitalsTest.goTo_ClickAndMeasureFID('clickImage',"https://www.dunelm.com/product/frea-cylinder-shade-15cm-1000169156",{x:location?.x,y:location?.y},'largest-contentful-paint')
      expect(results).to.have.property('FID').with.property('value').greaterThan(0);
    }
  });

  it('GoTo_ClickAndMeasureFID dom interactive', async () => {
      // To Find Website to test this
  });

});

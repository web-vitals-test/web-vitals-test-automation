import { expect } from 'chai';
import { WebVitalsTest } from '../../index';

describe('Scroll', function () {
  it('Desktop', async () => {
    await WebVitalsTest.getPage()?.goto('https://www.bbc.co.uk/', {
      waitUntil: 'networkidle2',
    });
    let scrollDown = await WebVitalsTest.scrollAndMeasure('ScrollDown',{pixels:{down:1000,right:0}});
    expect(scrollDown).to.have.property('CLS');
    expect(scrollDown.status).true
    let scrollUp = await WebVitalsTest.scrollAndMeasure('ScrollUp',{pixels:{down:-1000,right:0}});
    expect(scrollUp).to.have.property('CLS');
    expect(scrollUp.status).true
  });
});

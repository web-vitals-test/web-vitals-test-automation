import { Browser } from 'puppeteer-core';
import { WebVitalsTest } from '../../index';
import * as puppeteer from 'puppeteer';

let browser: unknown;
before(function () {
  WebVitalsTest.init({ port: 3001, mode: process.env.MODE || 'full' });
});

after(function () {
  WebVitalsTest.stopServer();
});

beforeEach(async function () {
  browser = await puppeteer.launch({
    headless: false,
    defaultViewport: { height: 900, width: 1440 },
    args: ['--start-maximized'],
  });
  await WebVitalsTest.setBrowser(browser as Browser);
  console.log("CPU is throttled")
});

afterEach(function () {
  (browser as Browser).close();
});

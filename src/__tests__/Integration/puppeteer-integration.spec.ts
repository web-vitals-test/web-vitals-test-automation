import { WebVitalsTest } from '../../index';
import {expect} from 'chai'
describe('puppeteer integration', function () {
  it('End to End - Desktop', async () => {
    WebVitalsTest.setCurrentScenario("BBC");
    const gotoResult = await WebVitalsTest.goToAndMeasure('goto','https://www.bbc.co.uk/', { waitUntil: 'networkidle2' });
    expect(gotoResult.LCP).exist
    expect(gotoResult.LCP).length(1)
    if(gotoResult.LCP)
      expect(gotoResult.LCP[0].value).greaterThan(0)
    let clickResult = await WebVitalsTest.clickAndMeasure('InitialLoad',{ value: '//*[@id="main-content"]//child::a',isXPath:true },{navigationOptions : {waitFor : 2000}} );
    expect(clickResult.LCP).exist
    if(clickResult.LCP)
      expect(clickResult.LCP[0].value).greaterThan(0)
    expect(clickResult.FID?.value).greaterThan(0)

    await WebVitalsTest.scrollAndMeasure('ScrollDown', { pixels : {down: 1000, right: 0 }});

    await WebVitalsTest.scrollAndMeasure('ScrollUp', { pixels : { down: -1000, right: 0 }});
    const scenarioResult = await WebVitalsTest.getResults("BBC")
    expect(scenarioResult).length(1)
    if(scenarioResult)
      expect(scenarioResult[0].getEntryList()[0].actionList).length(2)
    
  });

  it('End to End - mobile', async () => {
    WebVitalsTest.setCurrentScenario("Dunelm");

    const gotoResult = await WebVitalsTest.goToAndMeasure('goto','https://www.dunelm.com/', { waitUntil: 'networkidle2' });
    expect(gotoResult.LCP).exist
    expect(gotoResult.LCP?.length).greaterThan(0)
    if(gotoResult.LCP)
      expect(gotoResult.LCP[0].value).greaterThan(0)

    const clickSearch = await WebVitalsTest.clickAndMeasure('clickSearch',{ value: '[data-testid="headerSearchInput-desktop"]'},{navigationOptions : {waitFor : 2000}} );
    expect(clickSearch.LCP).exist
  
    if(clickSearch.LCP)
      expect(clickSearch.LCP[0].value).greaterThan(0)
    expect(clickSearch.FID?.value).greaterThan(0)

    await WebVitalsTest.scrollAndMeasure('ScrollDown', { pixels : {down: 1000, right: 0 }});

    await WebVitalsTest.scrollAndMeasure('ScrollUp', { pixels : {down: -1000, right: 0 }});
    
    const scenarioResult = await WebVitalsTest.getResults("Dunelm")
    expect(scenarioResult).length(1)
  });
});

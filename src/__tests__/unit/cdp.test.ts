import * as puppeteer from 'puppeteer';
import { CDP } from '../../Server/CDP';

describe('CDP', function () {
  it('send Message', async () => {
    const cdp = new CDP();
    const browser = await puppeteer.launch({
      headless: false,
      defaultViewport: { height: 1200, width: 1950 },
      args: ['--start-maximized'],
    });

    const page = (await browser.pages())[0];

    cdp.setwsEndpoint(browser.wsEndpoint());

    await cdp.sendClick({ x: 100, y: 100 });

    await new Promise((resolve) => setTimeout(resolve, 10000));

    browser.close();
  });
});

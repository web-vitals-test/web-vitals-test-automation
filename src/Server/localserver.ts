import * as express from 'express';
import { Express } from 'express';
import { Server } from 'http';
import { CDP } from './CDP';

export class LocalServer {
  static app: Express;
  static server: Server;
  static cdp: CDP;

  static startServer(port?: number) {
    LocalServer.cdp = new CDP();
    LocalServer.app = express();
    LocalServer.app.use(express.json());
    LocalServer.app.use(express.text());
    LocalServer.app.use(express.urlencoded({ extended: true }));

    LocalServer.app.post('/setwsEndPoint', (req, res) => {
      const jsonData = req.body;
      LocalServer.cdp.setwsEndpoint(jsonData.wsEndPoint || '');
      res.send('success');
    });

    LocalServer.app.post('/clickElement',async (req, res) => {
      try{
        const jsonData = JSON.parse(req.body);
        await LocalServer.cdp.sendClick({ x: jsonData.x, y: jsonData.y });
      }catch(e){
        res.send(e);
      }
      res.send('success');
    });

    LocalServer.server = LocalServer.app.listen(port || 3000);
  }

  static stopServer() {
    LocalServer.server.close();
  }
}

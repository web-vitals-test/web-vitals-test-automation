const WebSocket = require('ws');
const SEND = require('./SEND');

export class CDP {
  wsEndpoint = '';
  id = 1;
  setwsEndpoint(endpoint: string) {
    this.wsEndpoint = endpoint;
  }

  async sendClick(coordinates: { x?: number; y?: number }) {
    const ws = new WebSocket(this.wsEndpoint, { perMessageDeflate: false });
    await new Promise((resolve) => ws.once('open', resolve));

    ws.on('message', (msg: string) => JSON.parse(msg));
    const targetsResponse = await SEND(ws, {
      id: this.id++,
      method: 'Target.getTargets',
    });

    const pageTarget = targetsResponse.result.targetInfos.find((info: any) => info.type === 'page');

    const sessionId = (
      await SEND(ws, {
        id: this.id++,
        method: 'Target.attachToTarget',
        params: {
          targetId: pageTarget.targetId,
          flatten: true,
        },
      })
    ).result.sessionId;

    await SEND(ws, {
      sessionId,
      id: this.id++,
      method: 'Input.dispatchMouseEvent',
      params: {
        type: 'mouseMoved',
        x: coordinates.x,
        y: coordinates.y,
      },
    });

    await SEND(ws, {
      sessionId,
      id: this.id++,
      method: 'Input.dispatchMouseEvent',
      params: {
        type: 'mousePressed',
        x: coordinates.x,
        y: coordinates.y,
        button: 'left',
      },
    });

    await SEND(ws, {
      sessionId,
      id: this.id++,
      method: 'Input.dispatchMouseEvent',
      params: {
        type: 'mouseReleased',
        x: coordinates.x,
        y: coordinates.y,
        button: 'left',
      },
    });
  }
}

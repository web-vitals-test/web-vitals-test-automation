export class CustomeInjector {
  static getCssSelectorJS(mode?: string) {
    if (mode === 'compact' || mode === 'fidOnly') return '';
    return "\n \
        function getCssSelectorShort(el) { \n \
            let path = [], parent; \n \
            while (parent = el.parentNode) { \n \
              let tag = el.tagName, siblings; \n \
              path.unshift( \n \
                el.id ? `#${el.id}` : ( \n \
                  siblings = parent.children, \n \
                  [].filter.call(siblings, sibling => sibling.tagName === tag).length === 1 ? tag : \n \
                  `${tag}:nth-child(${1+[].indexOf.call(siblings, el)})` \n \
                ) \n \
              ); \n \
              el = parent; \n \
            }; \n \
            return `${path.join(' > ')}`.toLowerCase(); \n \
          }; \n ";
  }

  static getSendtoAnalyticsJS(port?: string, mode?: string) {
    let body;
    let listeners;
    let performanceObserservers="";
    if (mode === 'compact') {
      body =
        "if(metric.name == 'CLS'){ \n \
                if(metric.delta<0.0099) \n \
                            return \n \
              }\n \
              console.log(metric) \n \
              const analyticsData  = JSON.stringify({ name : metric.name , value : metric.value});";
      listeners =
        ' webVitals.getCLS(sendToAnalytics,true); \n \
                    webVitals.getFID(sendToAnalytics); \n \
                    webVitals.getLCP(sendToAnalytics); \n ';
    } else if (mode === 'fidOnly') {
      body =
        ' let newEntries = []; \n \
               newEntries.push({startTime : metric.entries[0].startTime}) \n \
               const analyticsData = { name : metric.name ,URL: window.location.href, actionName:localStorage["WebVitalsTest-ActionName"], value : metric.value, entries: newEntries}';
      listeners = 'webVitals.getFID(sendToAnalytics);';
    } else {
      body =
        "  \n \
            if(metric.name == 'LCP'){ \n \
                let newEntries =[]; \n \
                let lastEntry = metric.entries.pop(); \n \
                newEntries.push({loadTime : lastEntry.loadTime, renderTime: lastEntry.renderTime, url : lastEntry.url, id : lastEntry.id, cssSelector : getCssSelectorShort(lastEntry.element), location: lastEntry.element.getBoundingClientRect()}) \n \
                metric.entries = newEntries \n \
            } \n \
            if(metric.name == 'CLS'){ \n \
              console.log('CLS : ' , metric) \n \
                if(metric.delta<0.0099) \n \
                    return \n \
                let newEntries = []; \n \
                for(let i=0;i<metric.entries.length;i++){ \n \
                    if(!(metric.entries[i].sources[0]&&metric.entries[i].sources[0].hasOwnProperty('cssSelector'))){ \n \
                        let newSources = []; \n \
                        if(metric.entries[i].value>0.0099){ \n \
                            for(let j=0;j<metric.entries[i].sources.length;j++){ \n \
                                newSources.push({previousRect:metric.entries[i].sources[j].previousRect, cssSelector: getCssSelectorShort(metric.entries[i].sources[j].node)}) \n \
                            } \n \
                        } \n \
                        newEntries.push({value: metric.entries[i].value, sources : newSources}); \n \
                    } \n \
                    else{ \n \
                        newEntries.push(metric.entries[i]) \n \
                    } \n \
                } \n \
                metric.entries = newEntries \n \
            } \n \
            if(metric.name == 'FID'){ \n \
                let newEntries = []; \n \
                newEntries.push({startTime : metric.entries[0].startTime}) \n \
                metric.entries=newEntries \n \
            } \n \
            const analyticsEntry=[metric.entries[metric.entries.length - 1]] \n \
            const analyticsData = { name: metric.name, value : metric.value ,entries : analyticsEntry } \n \
            const body = JSON.stringify(analyticsData,null,2); \n ";

      listeners =
        ' webVitals.getCLS(sendToAnalytics,true); \n \
            webVitals.getFID(sendToAnalytics); \n \
            webVitals.getLCP(sendToAnalytics); \n '

      performanceObserservers =' \n \
          const lcpObserver = new PerformanceObserver((entryList) => { \n \
            const entries = entryList.getEntries() \n \
            const lastEntry = entries[entries.length-1] \n \
            const actionName = localStorage["WebVitalsTest-ActionName"] \n \
            const lcpEntry ={URL:window.location.href,actionName:actionName,value:lastEntry.startTime,loadTime : lastEntry.loadTime, renderTime: lastEntry.renderTime, url : lastEntry.url, id : lastEntry.id, cssSelector : getCssSelectorShort(lastEntry.element), location: lastEntry.element.getBoundingClientRect()} \n \
            console.log("LCP-PO",lcpEntry) \n \
            localStorage["LCP-PO"] = JSON.stringify(lcpEntry) \n \
          }); \n \
            lcpObserver.observe({type: "largest-contentful-paint", buffered: true}) \n \
          '
    }
    return (
      ' \n \
        function sendToAnalytics(metric) { \n \
            ' +
      body +
      ' \n \
          let currentData \n \
          if(localStorage[metric.name]){ \n \
            currentData = JSON.parse(localStorage[metric.name]) \n \
          } \n \
          let analyticsArray = currentData? currentData : [] \n \
          analyticsData["URL"] = window.location.href \n \
          if(analyticsData.name === "LCP" && localStorage["WebVitalsTest-PreviousAction"] !== "UNDEFINED" ){ analyticsData["actionName"] = localStorage["WebVitalsTest-PreviousAction"] } \n \
          else analyticsData["actionName"] = localStorage["WebVitalsTest-ActionName"] \n \
          analyticsArray.push(analyticsData) \n \
          console.log(analyticsData.name,analyticsData) \n \
          localStorage[metric.name] = JSON.stringify(analyticsArray)  \n \
          } \n \
          console.log(location.hostname) \n \
          if(location.hostname.length>0) { \n \
          ' +
          listeners + '\n' +
          performanceObserservers + '\n' +
          '}'
    );
  }

  static getclickEventPostPaintAPI_JS(port: string, coordinates : {x :number, y :number}, postEvent : 'first-contentful-paint' | 'largest-contentful-paint') {
    const waitFunction =
      "const myObserver = new PerformanceObserver(function(list) { \n \
                          for(const entry of list.getEntries()){ \n \
                            console.log(entry.name, entry.startTime) \n \
                              if(entry.name === '"+postEvent+"' && location.hostname.length>0){ \n \
                                myObserver.disconnect(); \n \
                                let elementData = {x :"+coordinates.x+" , y : "+coordinates.y+"}; \n \
                                const body = JSON.stringify(elementData,null,2); \n \
                                (navigator.sendBeacon && navigator.sendBeacon('http://localhost:" +
                                (port || '3000') +
                                "/clickElement', body)) || \n \
                                    fetch('http://localhost:" +
                                (port || '3000') +
                               "/clickElement', {body, method: 'POST', keepalive: true}); \n \
                              } \n \
                            } \n \
                        }); \n \
                      \n \
                      if(location.hostname.length>0){ \n \
                        console.log('registering paint api listener') \n \
                        myObserver.observe({entryTypes: ['paint']}); \n \
                      }";
    return waitFunction;
  }

  static getclickEventPostLCP_JS(port: string, coordinates : {x :number, y :number}) {
    const waitFunction =
      "const myObserver = new PerformanceObserver(function(list) { \n \
                          for(const entry of list.getEntries()){ \n \
                            console.log(entry.name, entry.startTime) \n \
                              if(location.hostname.length>0){ \n \
                                myObserver.disconnect(); \n \
                                let elementData = {x :"+coordinates.x+" , y : "+coordinates.y+"}; \n \
                                const body = JSON.stringify(elementData,null,2); \n \
                                (navigator.sendBeacon && navigator.sendBeacon('http://localhost:" +
                                (port || '3000') +
                                "/clickElement', body)) || \n \
                                    fetch('http://localhost:" +
                                (port || '3000') +
                               "/clickElement', {body, method: 'POST', keepalive: true}); \n \
                              } \n \
                            } \n \
                        }); \n \
                      \n \
                      if(location.hostname.length>0){ \n \
                        console.log('registering paint api listener') \n \
                        myObserver.observe({type: 'largest-contentful-paint', buffered: true}); \n \
                      }";
    return waitFunction;
  }

  static getclickEventDOMInteractive_JS(port: string, coordinates : {x :number, y :number}) {
    const waitFunction = "if(location.hostname.length>0){ \n \
                        console.log('registering ready state change event listener') \n \
                        document.addEventListener('readystatechange', () => { \n \
                          // The first readyStateChange will be : interactive \n \
                          // The second readyStateChange will be : complete \n \
                          console.log(document.readyState) \n \
                          if(document.readyState == 'interactive'){\n \
                            let elementData = {x :"+coordinates.x+" , y : "+coordinates.y+"}; \n \
                            const body = JSON.stringify(elementData,null,2); \n \
                            (navigator.sendBeacon && navigator.sendBeacon('http://localhost:" +
                            (port || '3000') +
                            "/clickElement', body)) || \n \
                                fetch('http://localhost:" +
                            (port || '3000') +
                           "/clickElement', {body, method: 'POST', keepalive: true}); \n \
                          } \n \
                        }); \n \
                      }"; 
    return waitFunction;
  }

}

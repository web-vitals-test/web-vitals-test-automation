import { Test } from "mocha";
import { ElementHandle, WaitForOptions } from "puppeteer-core";

export interface CLSData {
  URL:string,
  value: number;
  entries?: [{ sources: TestElement[] }];
  actionName?: string;
}

export interface TestElement {
  cssSelector?: string;
  location?: Location;
  newLocation?: Location;
  previousRect?: Location;
}

export interface FIDData {
  URL:string,
  value: number;
  entries?: [{ startTime?: number }];
  actionName?: string;
}

export interface LCPData {
  URL: string;
  url: string;
  loadTime: number;
  value:number;
  renderTime: number;
  id: string;
  cssSelector: string;
  location: Location;
  actionName?: string;
}

export interface WebVitalsTestResult {
  action: string;
  CLS: CLSData;
  FID: FIDData;
  LCP: LCPData;
}

export interface LcpAPIEntry {
  value: number;
  url: string;
  cssSelector: string;
  location: Location;
  loadTime: number;
  renderTime: number;
}

export interface LcpAPI {
  actionName: string;
  URL:string;
  value: number;
  entries?: LcpAPIEntry[];
  id?: string;
  action?: string;
}

export interface Location {
  x: number;
  y: number;
  width: number;
  height: number;
  top?: number;
  right?: number;
  bottom?: number;
  left?: number;
}

export interface CLS {
  actionName? : string;
  URL?:string;
  value: number;
  elements?: TestElement[];
  experience?: Experience;
  screenshot?: string;
}

export interface LCP {
  actionName?: string;
  URL?:string,
  elementURL?:string,
  loadTime?: number;
  value:number;
  renderTime?: number;
  element?: TestElement;
  experience?: Experience;
  screenshot?: string;
  engine?:'PO' | 'web-vitals';
}

export interface FID {
  actionName? : string;
  URL?:string,
  value: number;
  startTime?: number;
  experience?: Experience;
}

export interface ReportEntry {
  actionName?: string;
  status?: boolean;
  url?:string,
  CLS?: CLS[];
  FID?: FID;
  LCP?: LCP[];
  navigationTimings?: Timings;
  screenshot?: { before?: string; after?: string };
}

export interface URLReportLine {
  URL:string
  CLS?: CLS;
  LCP?: LCP;
  FID?: FID;
  actionList? : ReportLine[]
  navigationTimings?: Timings;
  screenshot?: string;
}

export interface ReportLine {
  actionName: string;
  status?:boolean;
  CLS?: CLS;
  LCP?: LCP;
  FID?: FID;
  navigationTimings?: Timings;
  screenshot?: string;
}

export interface Aggregation {
  [Experience.fast]: number;
  [Experience.average]: number;
  [Experience.poor]: number;
}

export enum Experience {
  fast = 'fast',
  average = 'average',
  poor = 'poor',
}

export interface WaitOptions {
  waitFor: number;
}

export interface Timings {
  requestStart: number;
  responseStart: number;
  responseEnd: number;
  domInteractive: number;
  domContentLoadedEventStart: number;
  domContentLoadedEventEnd: number;
  domComplete: number;
  loadEventStart: number;
  loadEventEnd: number;
}

export interface Emulation {
  name : string,
  userAgent? : string,
  viewport? : {
    width : number,
    height : number,
    isMobile : boolean,
    hasTouch : boolean,
    isLandscape : boolean
    deviceScaleFactor : number
  }
  network? : {
    offline : boolean,
    downloadThroughput : number,
    uploadThroughput : number,
    latency : number
  }
  cpuThrottle? : {rate : number}
}

export interface Selector{
   value: string; 
   isXPath?: boolean;
}

export type NavigationWaitOptions = WaitForOptions | WaitOptions | {waitForElement : Selector}
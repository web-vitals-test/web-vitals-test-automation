
# web-vitals-test

### Test Automation for Web Vitals

This typescript package enables you to automate testing for Web Vitals (https://web.dev/). It relies on web vitals Library and capabilities of puppeteer automation tool for browsers. 

### How to use
#### Initialization

Express server is run in the background on port 3000. It can be overridden using init method. Ensure that through out execution it is only called once.

    //import {WebVitalsTest as Controller} from  'web-vitals-test';
    Controller.init({port:3000});;

#### Pass puppeteer Page to Controller
Send Puppeteer Page object to Controller so that it can track web vital metrics and further Measure methods can be executed.
    
    const  browser = await  puppeteer.launch({headless:false,defaultViewport:{height:1200,width:1950},args:['--start-maximized' ]});
    await  Controller.setBrowser(browser);

#### Navigate to URL
Use inbuilt method to open URL. Action name is mandatory, that will be used in the HTML report. By design, only CLS data is available at page load.

     let result = await Controller.goTo_ClickAndMeasure(
      'http://automationpractice.com/',
      'InitialLoad',
      { value: '[id="search_query_top"]' },
      { waitFor: 1000 },
    );

OR


     let result = await Controller.goToAndMeasure('http://automationpractice.com/', 'InitialLoad', {
      waitUntil: 'networkidle2',
    });
    

#### Interaction with Browser
Currently click, select and scroll events are supported to measure the metrics, For all the other interaction, use puppeteer methods. 

    await Controller.clickAndMeasure('ClickElement',{ value : '[id="search_query_top"]' });

    await  Controller.scrollAndMeasure('ScrollDown',{down:  1000,right:0});
    await  Controller.scrollAndMeasure('ScrollUp',{down: -1000,right:0});

    await Controller.selectAndMeasure('SelectPrice',{ value: '[id="group_1"]'},'3')

    await WebVitalsTest.typeAndMeasure('type',{ value: '#sli_search_1' },{text:'shirt',pressEnter:true},{waitUntil:'networkidle2'} );

#### Report
Report is available in JSON Format

Web-Vitals-Test-Reporter can be used in the conjuction to generate HTML report.

## Release History
### 4.1.0
    Breaking Changes - Removed LSN as new CLS definition is based on LSN.
### 3.1.10
    add option not to create report
### 3.1.8
    Bugfix - network and cpu emulation
### 3.1.7
    add hoverAndMeasure
### 3.1.6
    no process event listener leak and detach CDP session after setting emulation and cpu throttle
### 3.1.5
    bug fixes for typeAndMeasure and status
### 3.1.4
    add error status in action
### 3.1.1
    make reporter global so that multiple controllers can be used parallelly.
### 3.0.11
    add deviceScaleFactor in emulation , add optional think time to wait before metrics are measued  and fix scrollUp bug that was not working
### 3.0.10
    Introduced TypeAndMeasure and exposed Controller object for parallel execution
### 3.0.9
    Correct action name for LCP PO reporting on page load
### 3.0.1 -> 3.0.8
    Bug fixes and support for LCP PO and LCP web vitals both
### 3.0.0
    support for LSN (Layout Shift Normalized) metric that would replace CLS